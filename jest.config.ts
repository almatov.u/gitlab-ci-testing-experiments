import type { Config } from '@jest/types';

const config: Config.InitialOptions = {
  preset: 'ts-jest',
  transform: {
    '^.+\\.ts$': [
      'ts-jest',
      {
        isolatedModules: true,
      },
    ],
  },
  moduleFileExtensions: ['js', 'json', 'ts'],
  testRegex: '.*\\.spec\\.ts$',
  collectCoverage: true,  // collecting coverage report
  collectCoverageFrom: [
    './src/**/*.ts',      // including files...
    '!**/testHelpers/**', // excluding files...
  ],
  coverageDirectory: './coverage',
  testEnvironment: 'node',
};

// Imagine simple flow to implement some cool feature.
// - Get a new "feature/cool-feature-1" branch from the "main" branch
// - Do some changes on the "feature/cool-feature-1" branch and commit them
// - Create a merge request to merge the changes from the "feature/cool-feature-1" to "main"
// In this case GitLab variables contains the following values:
//   CI_MERGE_REQUEST_SOURCE_BRANCH_NAME = feature/cool-feature-1
//   CI_MERGE_REQUEST_TARGET_BRANCH_NAME = main
// So, to run the tests of the changed modules since the "main" set the Jest's "changedSince" option:
if (process.env.CI_MERGE_REQUEST_TARGET_BRANCH_NAME) {
  config.changedSince = process.env.CI_MERGE_REQUEST_TARGET_BRANCH_NAME;
}

export default config;
