export function add(a: number, b: number) {
  return a + b;
}

export function specialAdd(a: number, b: number) {
  // this if statement intentionally not covered with tests
  if (a < 0 || b < 0) {
    return 0;
  }

  return a + b;
}
