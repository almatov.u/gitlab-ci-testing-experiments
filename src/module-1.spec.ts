import { add, specialAdd } from './module-1';

describe('add', () => {
  it('1 + 2 = 3', () => {
    expect(add(1, 2)).toBe(3);
  });
});

describe('specialAdd', () => {
  it('1 + 2 = 3', () => {
    expect(specialAdd(1, 2)).toBe(3);
  });
});
